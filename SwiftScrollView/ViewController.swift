//
//  ViewController.swift
//  SwiftScrollView
//
//  Created by Canyon Duncan on 9/8/17.
//  Copyright © 2017 Canyon Duncan. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let scrollView = UIScrollView(frame:self.view.bounds)
        scrollView.backgroundColor = UIColor.cyan
        self.view.addSubview(scrollView)
        scrollView.isPagingEnabled = true
        scrollView.contentSize = CGSize(width: self.view.frame.size.width*3, height: self.view.frame.size.height*3)
        
        let view1 = UIView(frame: CGRect(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height))
        view1.backgroundColor = UIColor.black
        scrollView.addSubview(view1)
        
        let view2 = UIView(frame: CGRect(x: 0, y: self.view.frame.size.height*2, width: self.view.frame.size.width, height: self.view.frame.size.height))
        view2.backgroundColor = UIColor.green
        scrollView.addSubview(view2)
        
        let view3 = UIView(frame: CGRect(x: self.view.frame.size.width, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        view3.backgroundColor = UIColor.brown
        scrollView.addSubview(view3)
        
        let view4 = UIView(frame: CGRect(x: self.view.frame.width, y: self.view.frame.height, width: self.view.frame.width, height: self.view.frame.height))
        view4.backgroundColor = UIColor.magenta
        scrollView.addSubview(view4)
        
        let view5 = UIView(frame: CGRect(x: self.view.frame.width, y: self.view.frame.height*2, width: self.view.frame.width, height: self.view.frame.height))
        view5.backgroundColor = UIColor.purple
        scrollView.addSubview(view5)
        
        let view6 = UIView(frame: CGRect(x: self.view.frame.width*2, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        view6.backgroundColor = UIColor.gray
        scrollView.addSubview(view6)
        
        let view7 = UIView(frame: CGRect(x: self.view.frame.width*2, y: self.view.frame.height, width: self.view.frame.width, height: self.view.frame.height))
        view7.backgroundColor = UIColor.red
        scrollView.addSubview(view7)
        
        let view8 = UIView(frame: CGRect(x: self.view.frame.width*2, y: self.view.frame.height*2, width: self.view.frame.width, height: self.view.frame.height))
        view8.backgroundColor = UIColor.orange
        
        scrollView.addSubview(view8)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

